#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

function print_usage {
	local cmdline="${1:-primerun}"
	printf 'Usage: %s [-i INDEX] [-u] -- COMMAND\n' "$cmdline"
}

index=1
verbosity=0
while getopts ":i:vuh" opt; do
	case ${opt} in
		i)
			index=$OPTARG
			;;
		v)
			verbosity=$((verbosity + 1))
			;;
		u)
			index=-1
			;;
		h)
			print_usage "$0"
			exit 0
			;;
		\?)
			echo "Unknown argument: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Invalid argument: -$OPTARG requires an argument" >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

function filter_env {
	local opt
	local OPTARG
	local OPTIND
	local exact=0
	while getopts ":e" opt; do
		case ${opt} in
			e)
				exact=1
				;;
			\?)
				echo "Unknown argument: -$OPTARG" >&2
				return 1
				;;
			:)
				echo "Invalid argument: -$OPTARG requires an argument" >&2
				return 1
				;;
		esac
	done
	shift $((OPTIND - 1))
	local pattern="${1:-"^.*$"}"
	[ $exact -eq 0 ] || pattern="$(printf '^%s$' "$pattern")"
	if [ $(awk -F '=' "{ if (\$1 ~ /$pattern/) { print \$0; } }" | wc -l) -le 0 ]; then
		printf '\033[1;36m%s\033[0m was unset\n' 'DRI_PRIME'
	fi
	awk -F '=' "{ if (\$1 ~ /$pattern/) { print \$0; } }"
}

[ $verbosity -lt 2 ] || set -x

if [ $index -ge 0 ]; then
	export DRI_PRIME=$index
else
	unset DRI_PRIME
fi
[ $verbosity -lt 1 ] || (env | filter_env -e DRI_PRIME >&2)
exec "$@"
